<?php
/**
 * Created by PhpStorm.
 * User: myrka
 * Date: 11.11.17
 * Time: 19:01
 */

namespace MaxTestBundle\Statistic;


use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class BehaviorDeep
{
    const EQUAL_PARTS    = 10;
    const BEHAVIOR_DEEP  = 3;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var $connection Connection
     */
    private $connection;

    /**
     * @var Container $container
     */
    private $container;

    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->em         = $entityManager;
        $this->connection = $this->em->getConnection();
        $this->container  = $container;
    }

    /**
     * @description: the method returns the number of records from the log file grouped by user
     * @return int
     */
    private function getLogCount()
    {
        $count = $this->connection->executeQuery(
            'SELECT count(*) cnt from (SELECT user_id, count(*) behavior_cnt FROM log GROUP BY user_id) a'
        )->fetch(\PDO::FETCH_COLUMN);
        return $count ?: 0;
    }

    /**
     * @param $count
     * @param $divider
     * @return float|int
     */
    private function getEqualParts($count, $divider)
    {
        if (!$count%$divider) {
            return $count/$divider;
        }
        return ceil(($count+($divider-($count%$divider)))/$divider);
    }

    private function truncateHashes()
    {
        $this->connection->exec('truncate hashes');
    }

    /**
     * @description: call mysql procedure and wait end of work
     */
    public function addTaskForProcedure()
    {
        $count = $this->getLogCount();
        $part  = $this->getEqualParts($count, self::EQUAL_PARTS);

        $this->truncateHashes();

        $client = new \GearmanClient();
        $client->addServer();

        $queueName = $this->container->getParameter('queue_behavior_deep_name');

        for ($i = 0; $i < self::EQUAL_PARTS; $i++) {
            $offset = $i == 0 ? 0 : $part * $i;
            $data = new ProcedureData();
            $data->setLimit($part)
                 ->setOffset($offset)
                 ->setBehaviorDeep(self::BEHAVIOR_DEEP);
            $data = serialize($data);
            $client->addTask($queueName, $data, null, $i);
        }
        $client->runTasks();
    }

    /**
     * @description: front data
     * @return array
     */
    public function getStatisticJuice()
    {
        $stmt = $this->connection->prepare('SELECT behavior, cnt from hashes ORDER BY cnt DESC LIMIT 10');
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}