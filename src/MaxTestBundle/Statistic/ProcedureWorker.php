<?php
/**
 * Created by PhpStorm.
 * User: myrka
 * Date: 14.11.17
 * Time: 12:57
 */

namespace MaxTestBundle\Statistic;


use Doctrine\ORM\EntityManager;

class ProcedureWorker
{
    /** @var ProcedureData $data */
    private $data;

    /** @var EntityManager $em */
    private $em;

    /** @var \Doctrine\DBAL\Connection $connection */
    private $connection;


    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->connection = $this->em->getConnection();
    }

    /**
     * @param ProcedureData $data
     */
    public function setProcedureData(ProcedureData $data)
    {
       $this->data = $data;
    }

    public function callProcedure()
    {
        $stmt = $this->connection->prepare('CALL user_behavior_statistic(:behavior_deep, :offset, :limit)');
        $stmt->execute(
            [
                'behavior_deep' => $this->data->getBehaviorDeep(),
                'offset'        => $this->data->getOffset(),
                'limit'         => $this->data->getLimit()
            ]
        );
    }
}