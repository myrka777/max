<?php
/**
 * Created by PhpStorm.
 * User: myrka
 * Date: 11.11.17
 * Time: 13:39
 */

namespace MaxTestBundle\Statistic;

use Doctrine\ORM\EntityManager;
use MaxTestBundle\Entity\FileUpload;
use Symfony\Component\Filesystem\Filesystem;

class FileConverter
{
    const TABLE_LOG_NAME = 'log';
    const INSERT_LIMIT   = 10000;
    const BYTES_LIMIT    = 1024;
    const CSV_DELIMITER  = ';';

    /**
     * @var EntityManager
     */
    private $em;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @description: import file data to log table
     * @param FileUpload $fileUpload
     */
    public function convertFileDataToDB(FileUpload $fileUpload)
    {
        $connection = $this->em->getConnection();
        $fileName   = $fileUpload->getFullFilePath();
        if ($handle = fopen($fileName, 'r')) {
            $tableName = self::TABLE_LOG_NAME;

            $connection->exec("TRUNCATE $tableName");

            $sql    = "INSERT INTO $tableName (`time_to_connect`, user_id, page_id) VALUES ";
            $i      = 0;
            $values = [];
            while (($line = fgetcsv($handle, self::BYTES_LIMIT, self::CSV_DELIMITER)) !== FALSE) {
                if (isset($line[0]) && isset($line[1]) && isset($line[2])) {
                    $line = array_map('intval', $line);
                    $values[] = '('.$line[0].','.$line[1].','.$line[2].')';
                }

                if ($i === self::INSERT_LIMIT) {
                    $connection->executeUpdate($sql . implode(',', $values));
                    $i      = 0;
                    $values = [];
                }
                $i++;
            }
            if ($values) {
                $connection->executeQuery($sql . implode(',', $values));
            }
            fclose($handle);
        }
        $filesystem = new Filesystem();
        $filesystem->remove($fileName);
    }
}
