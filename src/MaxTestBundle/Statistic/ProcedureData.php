<?php
/**
 * Created by PhpStorm.
 * User: myrka
 * Date: 14.11.17
 * Time: 12:11
 */

namespace MaxTestBundle\Statistic;


class ProcedureData
{
    /** @var  int */
    private $limit;

    /** @var  int */
    private $offset;

    /** @var  int */
    private $behaviorDeep;

    /**
     * @return int
     */
    public function getLimit()
    {
        return (int) $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return (int) $this->offset;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return int
     */
    public function getBehaviorDeep()
    {
        return (int) $this->behaviorDeep;
    }

    /**
     * @param int $behaviorDeep
     * @return $this
     */
    public function setBehaviorDeep($behaviorDeep)
    {
        $this->behaviorDeep = $behaviorDeep;
        return $this;
    }
}