<?php

namespace MaxTestBundle\Controller;

use MaxTestBundle\Entity\FileUpload;
use MaxTestBundle\Form\FileUploadType;
use MaxTestBundle\Statistic\BehaviorDeep;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FileUploadController extends Controller
{
    public function indexAction()
    {
        $form = $this->createForm(FileUploadType::class);

        return $this->render('MaxTestBundle:FileUpload:index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadAction(Request $request)
    {
        $fileUpload = new FileUpload();
        /** @var $form Form */
        $form = $this->createForm(FileUploadType::class, $fileUpload);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest() && $form->isSubmitted() && $form->isValid()) {

            try {
                $result = $this->uploadSteps($fileUpload);
            } catch (\InvalidArgumentException $e) {
                return new JsonResponse(['error_message' => $e->getMessage()]);
            }

            return new JsonResponse(['message' => $result]);
        }

        $error = $this->getErrorMessages($form);

        if ($error && array_key_exists(FileUpload::FILE_UPLOAD_NAME, $error) && $error[FileUpload::FILE_UPLOAD_NAME]) {
            return new JsonResponse(['error_message' => $error[FileUpload::FILE_UPLOAD_NAME]]);
        }

        return new JsonResponse(['error_message' => FileUpload::FILE_UPLOAD_UNDEFINED_ERROR_MESSAGE]);
    }

    /**
     * @description: for MimeType error
     * @param Form $form
     * @return array
     */
    private function getErrorMessages(Form $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        /**
         * @var $child Form
         */
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

    /**
     * @param FileUpload $fileUpload
     * @return array
     */
    private function uploadSteps(FileUpload $fileUpload)
    {
        /**
         * @var $fileUploadService \MaxTestBundle\Service\FileUpload
         */
        $fileUploadService = $this->container->get('max_test.file_upload');
        $fileUploadService->uploadCSVFile($fileUpload);

        /**
         * @var $fileConverter \MaxTestBundle\Statistic\FileConverter
         */
        $fileConverter = $this->container->get('max_test.file_converter');
        $fileConverter->convertFileDataToDB($fileUpload);

        /**
         * @var $behaviorDeep BehaviorDeep
         */
        $behaviorDeep = $this->container->get('max_test.behavior_deep');
        $behaviorDeep->addTaskForProcedure();

        return $behaviorDeep->getStatisticJuice();
    }
}
