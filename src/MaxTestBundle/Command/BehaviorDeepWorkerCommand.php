<?php

namespace MaxTestBundle\Command;

use MaxTestBundle\Statistic\ProcedureData;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BehaviorDeepWorkerCommand extends ContainerAwareCommand
{
    const OFFSET_OPT_NAME         = 'offset';
    const LIMIT_OPT_NAME          = 'limit';
    const BEHAVIOR_DEEP_OPT_NAME  = 'behavior-deep';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('max_test:behavior_deep_worker_command')
            ->setDescription('Run mysql procedure user_behavior_statistic with options.')
            ->addOption(
                self::BEHAVIOR_DEEP_OPT_NAME,
                'bd',
                InputOption::VALUE_OPTIONAL,
                'Set behavior deep for statistic.'
            )->addOption(
                self::OFFSET_OPT_NAME,
                'o',
                InputOption::VALUE_OPTIONAL,
                'Set procedure offset.'
            )->addOption(
                self::LIMIT_OPT_NAME,
                'l',
                InputOption::VALUE_OPTIONAL,
                'Set procedure limit.'
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $behaviorDeep = $input->getOption(self::BEHAVIOR_DEEP_OPT_NAME);
        $offset       = $input->getOption(self::OFFSET_OPT_NAME);
        $limit        = $input->getOption(self::LIMIT_OPT_NAME);

        $data = null;

        if (isset($behaviorDeep) && isset($offset) && isset($limit)) {
            $data = new ProcedureData();
            $data->setLimit($limit)
                 ->setOffset($offset)
                 ->setBehaviorDeep($behaviorDeep);
            $this->callProcedure($data);
        } else {
            $this->gearmanRun();
        }
    }

    private function gearmanRun()
    {
        $queueName = $this->getContainer()->getParameter('queue_behavior_deep_name');

        $worker= new \GearmanWorker();
        $worker->addServer();
        $worker->addFunction($queueName, [$this, 'gearmanJob']);
        while ($worker->work());
    }

    public function gearmanJob(\GearmanJob $job)
    {
        $serializeData = $job->workload();
        $data = unserialize($serializeData);
        $this->callProcedure($data);
    }

    private function callProcedure(ProcedureData $data)
    {
        $worker = $this->getContainer()->get('max_test.procedure_worker');
        $worker->setProcedureData($data);
        $worker->callProcedure();
    }
}
