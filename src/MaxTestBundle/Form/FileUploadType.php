<?php

namespace MaxTestBundle\Form;

use MaxTestBundle\Entity\FileUpload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileUploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fileUpload', FileType::class, ['label'=>'Log (csv file)', 'attr'=>['class'=>'file_uploader']])
            ->add('save', SubmitType::class, ['label' => 'Get juice', 'attr'=>['class'=>'file_uploader_btn']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => FileUpload::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'max_test_bundle_file_upload_type';
    }
}
