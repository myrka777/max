/**
 * Created by myrka on 10.11.17.
 */
$(function(){
    $('.js-form_file_upload').on('submit', function (e) {
        $('h2.hidden').removeClass('hidden');
        $('.file_uploader_btn').prop('disabled', true);
        e.preventDefault();
        var dot = 'please wait ',
            interval_function_id;
        interval_function_id = setInterval(function () {
            dot += '.';
            if (dot.length > 20) {
                dot = 'please wait ';
            }
            $('#upload-result-container').html(dot);
        }, 500);

        $.ajax({
            url: '/upload',
            data: new FormData($(this)[0]),
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                $('.file_uploader_btn').prop('disabled', false);
                clearInterval(interval_function_id);
                if (data.error_message) {
                    alert(data.error_message);
                    return;
                }

                var msg_length = data.message.length,
                    i = 0,
                    html = '';
                for (i; i < msg_length; i++) {
                    html += '<tr><td>' + data.message[i].cnt + '</td><td>' + data.message[i].behavior + '</td></tr>';
                }
                if (!html) {
                    html = '<tr><td>No result</td></tr>'
                }
                $('#upload-result-container').html(html);
            }, error: function () {
                alert('ajax error!');
            }
        });
    });
});