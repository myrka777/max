<?php
/**
 * Created by PhpStorm.
 * User: myrka
 * Date: 09.11.17
 * Time: 18:18
 */

namespace MaxTestBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class FileUpload
{
    const MIME_TYPE_CSV                       = 'text/csv';
    const FILE_UPLOAD_NAME                    = 'fileUpload';
    const FILE_UPLOADED_MESSAGE               = 'File Uploaded!';
    const FILE_UPLOAD_UNDEFINED_ERROR_MESSAGE = 'Upload file undefined error!';

    /**
     * @Assert\NotBlank(message="File could`t be empty.")
     * @Assert\File(mimeTypes={ "text/plain" }, maxSize = "300M",)
     */
    private $fileUpload;

    private $fullFilePath;

    /**
     * @return mixed
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }

    /**
     * @param $fileUpload
     * @return $this
     */
    public function setFileUpload($fileUpload)
    {
        $this->fileUpload = $fileUpload;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullFilePath()
    {
        return $this->fullFilePath;
    }

    /**
     * @param $fullFilePath
     * @return $this
     */
    public function setFullFilePath($fullFilePath)
    {
        $this->fullFilePath = $fullFilePath;
        return $this;
    }
}
