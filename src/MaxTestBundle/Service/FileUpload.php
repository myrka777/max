<?php
/**
 * Created by PhpStorm.
 * User: myrka
 * Date: 09.11.17
 * Time: 22:08
 */

namespace MaxTestBundle\Service;

use MaxTestBundle\Entity\FileUpload as FileUploadEntity;
use Symfony\Component\DependencyInjection\Container;

class FileUpload
{
    private $container;

    /**
     * FileUpload constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param FileUploadEntity $uploadEntity
     */
    public function uploadCSVFile(FileUploadEntity $uploadEntity)
    {
        $file = $uploadEntity->getFileUpload();
        if ($file->getClientMimeType() === FileUploadEntity::MIME_TYPE_CSV) {
            $originalUploadFileName = $file->getClientOriginalName();
            $originalUploadFileName = str_replace(
                '.' . $file->getClientOriginalExtension(),
                '',
                $originalUploadFileName
            );

            $fileName = $originalUploadFileName . '_' . round(microtime(true) * 1000)
                        . '.' . $file->getClientOriginalExtension();
            $file->move(
                $this->container->getParameter('file_upload_directory'),
                $fileName
            );
            $uploadEntity->setFullFilePath($this->container->getParameter('file_upload_directory').$fileName);
            $uploadEntity->setFileUpload($fileName);
        } else {
            throw new \InvalidArgumentException(
                'The mime type of the file is invalid. Allowed mime types are "text/plain".'
            );
        }
    }
}
