create table hashes
(
	id int not null auto_increment
		primary key,
	cnt int default '1' not null,
	behavior varchar(1000) not null,
	hash char(32) not null,
	constraint hashes_hash_uindex
		unique (hash)
);
create index hashes_cnt_index	on hashes (cnt);



create table log
(
	id int not null auto_increment
		primary key,
	time_to_connect int not null,
	user_id int not null,
	page_id int not null
);
create index log_user_id_index on log (user_id);





DELIMITER //
CREATE PROCEDURE user_behavior_statistic(IN behavior_deep INT, IN user_offset INT, IN user_limit INT)
  BEGIN
    DECLARE done_users INT DEFAULT 0;
    DECLARE u, c INT;
    DECLARE i INT DEFAULT 0;
    DECLARE hc INT;
    DECLARE b, h VARCHAR(1000);

    DECLARE user_cursor CURSOR FOR SELECT user_id, count(*) behavior_cnt FROM log GROUP BY user_id LIMIT user_offset, user_limit;
    DECLARE hash_cursor CURSOR FOR SELECT
                                     count(sub.id)                                hash_cnt,
                                     group_concat(sub.page_id SEPARATOR ',')      behavior,
                                     md5(group_concat(sub.page_id SEPARATOR ',')) hash
                                   FROM (SELECT
                                           id,
                                           page_id
                                         FROM log
                                         WHERE user_id = u
                                         LIMIT i, behavior_deep) sub;

    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done_users = 1;

    OPEN user_cursor;

    WHILE done_users = 0 DO
      FETCH user_cursor into u, c;
      IF done_users = 0 THEN
        IF c >= behavior_deep THEN

          REPEAT
            OPEN hash_cursor;
            FETCH hash_cursor into hc, b, h;
            IF hc >= behavior_deep THEN
              SET i = i + 1;
              INSERT INTO hashes (behavior, `hash`) VALUES (b, h) ON DUPLICATE KEY UPDATE cnt = cnt + 1;
              CLOSE hash_cursor;
            ELSE
              SET i = 0;
              CLOSE hash_cursor;
            END IF ;
          UNTIL hc < behavior_deep END REPEAT ;

        END IF;
      END IF;
    END WHILE ;
    CLOSE user_cursor;

  END
//

DELIMITER ;