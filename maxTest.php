<?php

function squeeze($fileName, $deep = 3) {
    $handle = fopen($fileName, 'r');

    $userBehavior = [];
    $maxResult    = [];

    while (($line = fgetcsv($handle, 512, ';')) !== FALSE) {
        $userBehavior[$line[1]][] = $line[2];

        $behaviorArray = &$userBehavior[$line[1]];

        if (count($behaviorArray) === $deep) {
            $behavior = implode('->', $behaviorArray);
            $maxIndex = &$maxResult[$behavior];

            isset($maxIndex) ? $maxIndex++ : $maxIndex = 1;

            array_shift($behaviorArray);
        }
    }

    fclose($handle);

    $maxResult = array_flip($maxResult);
    $maxIndex  = max(array_keys($maxResult));

    return $maxResult[$maxIndex];
}